import 'package:testes/Seguradoras/Automotive.dart';
import 'package:testes/Seguradoras/House.dart';
import 'package:testes/Seguradoras/insurance.dart';
import 'package:testes/Seguradoras/insuranceCompany.dart';
import 'package:testes/Seguradoras/insured.dart';
import 'package:testes/Seguradoras/owner.dart';
import 'Seguradoras/health.dart';

List<House> createHouseList() {
  List<House> insList = [];

  insList.add(House(
    'ho000001',
    DateTime(2010 - 11 - 05),
    1500,
    true,
    45000,
    250000,
    'Rue Straight nº9',
    paymentFrequency: "monthly",
    'apartment',
    Owner('Francis', 250111454, 'Rue Straight nº9', 24, 0000144455524585244),
    InsuranceCompany('Liberty City', 504112488),
  ));

  insList.add(House(
    'ho000221',
    DateTime(2021 - 02 - 14),
    788,
    true,
    19000,
    198000,
    paymentFrequency: "monthly",
    'Avenue Wall Street nº14',
    'Bungalow',
    Owner(
        'Richard', 114088977, 'Avenue Wall Street nº14', 45, 00001333478995252),
    InsuranceCompany('ActivoBank Insurance', 548445113),
  ));

  insList.add(House(
      'ho000124',
      DateTime(2016 - 10 - 25),
      1100,
      true,
      26000,
      154800,
      paymentFrequency: "yearly",
      'Rue de Cima nº954',
      'Single Storey House',
      Owner('Francis', 244522488, 'Rue de Cima nº954', 28, 00002665411114455),
      InsuranceCompany('Tranquilidade', 501415778)));

  return insList;
}

List<Health> createHealthList() {
  List<Health> healthList = [];

  healthList.add(Health(
      "he0015151100",
      DateTime(2010 - 04 - 12),
      255,
      true,
      15000,
      "approved",
      5000,
      paymentFrequency: "monthly",
      Owner("John", 104222333, 'Travessa das Costas nº22', 49,
          0000312458744122214),
      InsuranceCompany("Liberty City", 504116477),
      insured: Insured(
          "Mariah", 445142223, 'Travessa das Costas nº22', 45, "Wife")));

  healthList.add(Health(
      "he0015151101",
      DateTime(2010 - 04 - 12),
      255,
      true,
      15000,
      "approved",
      5000,
      paymentFrequency: "monthly",
      Owner("John", 104222333, 'Travessa das Costas nº22', 49,
          0000312458744122214),
      InsuranceCompany("Liberty City", 504116477),
      insured:
          Insured("John", 104222333, 'Travessa das Costas nº22', 49, "Owner")));

  healthList.add(Health(
      "he0015151102",
      DateTime(2010 - 04 - 12),
      255,
      true,
      15000,
      "approved",
      5000,
      paymentFrequency: "monthly",
      Owner("John", 104222333, 'Travessa das Costas nº22', 49,
          0000312458744122214),
      InsuranceCompany("Liberty City", 504116477),
      insured: Insured(
          "Patrick", 445142223, 'Travessa das Costas nº22', 14, "Son")));

  return healthList;
}

List<Automotive> createListAutomotive() {
  List<Automotive> autoList = [];

  autoList.add(Automotive(
      "au0001883",
      DateTime(2010 - 08 - 26),
      998,
      true,
      4500,
      Owner("Rodry", 12245866, 'Rua Professor Manuel nº345', 33,
          0000156165165412344),
      InsuranceCompany("Ok TeleSeguros", 504112335),
      paymentFrequency: "yearly",
      "mercedes",
      "th-77-14",
      "AF44451478",
      usuallyDriver: Insured(
          "Rodry", 12245866, 'Rua Professor Manuel nº345', 33, "nephew")));

  autoList.add(Automotive(
      "au0001233",
      DateTime(2009 - 04 - 22),
      877,
      true,
      9000,
      Owner("Sebastian", 104555666, 'Rua Sentença de Cima', 41,
          0000115515198123156),
      InsuranceCompany("Tranquilidade", 254888996),
      paymentFrequency: "monthly",
      "bmw",
      "ah-77-rh",
      "AF1231321312",
      usuallyDriver:
          Insured("Filipe", 215444222, 'Avenida Gomes Juvenil', 56, "nephew")));

  autoList.add(Automotive(
      "au007745555",
      DateTime(2018 - 06 - 44),
      455,
      true,
      14000,
      Owner("Tristan", 189444775, 'Avenida Christopher Down', 62,
          000156165123188898),
      InsuranceCompany("Continente Seguros", 504113288),
      paymentFrequency: "yearly",
      "Honda",
      "bz-44-ut",
      "AM001993125",
      usuallyDriver: Insured(
          "Sophie", 189855215, 'Avenida Christopher Down', 48, "sister")));

  return autoList;
}

void printList(list) {
  for (var e in list) {
    print(e);
  }
}

List<Insurance> getActivePolicies(list) {
  List<Insurance> result = [];
  for (var e in list) {
    if (e.status) {
      result.add(e);
    }
  }
  return result;
}

int activePoliciesQty(list) {
  List<Object> result = [];
  for (var e in list) {
    if (e.status) {
      result.add(e);
    }
  }
  return result.length;
}

int inactivePoliciesQty(list) {
  List<Object> result = [];
  for (var e in list) {
    if (!e.status) {
      result.add(e);
    }
  }
  return result.length;
}

int getAllPoliciesQty(list) {
  return list.length;
}

double getCoverageAmountAverage(list) {
  double result = 0;
  int qty = 0;
  for (var e in list) {
    if (e.status) {
      result += e.coverageAmount;
      qty++;
    }
  }

  return (result / qty).roundToDouble();
}

void getYearlySummaryByType(list, type) {
  List<Insurance> result;
  double totalSum = 0;

  print("\nInsurance Type: $type");

  result = getActivePolicies(list);
  for (var e in result) {
    if (e.runtimeType == type) {
      double yearPremium =
          e.paymentFrequency == "yearly" ? e.insPremium : e.insPremium * 12;
      totalSum += yearPremium;

      print("Policy nº: ${e.policy}  || Yearly premium: $yearPremium");
    }
  }
  print("Total Amount: $totalSum");
}

void getYearlySummaryByCompany(list, companyName) {
  List<Insurance> result;
  double totalSum = 0;

  result = getActivePolicies(list);

  print("\nCompany name: $companyName");
  for (var e in result) {
    if (e.company.name == companyName) {
      double yearPremium =
          e.paymentFrequency == "yearly" ? e.insPremium : e.insPremium * 12;
      totalSum += yearPremium;

      print("Policy nº: ${e.policy}  || Yearly premium: $yearPremium");
    }
  }
  print("Total Amount: $totalSum\n");
}

void printYearlySummaryByType(list) {
  List typesList = [];
  for (var e in list) {
    if (!typesList.contains(e.runtimeType)) {
      typesList.add(e.runtimeType);
    }
  }
  for (var i in typesList) {
    getYearlySummaryByType(list, i);
  }
}

void printYearlySummaryByCompany(list) {
  List companiesList = [];
  for (var e in list) {
    if (!companiesList.contains(e.company.name)) {
      companiesList.add(e.company.name);
    }
  }
  for (var i in companiesList) {
    getYearlySummaryByCompany(list, i);
  }
}

// Insured
void printSummaryByInsured(list, insuredName) {
  List<Insurance> result;
  double totalSum = 0;

  result = getActivePolicies(list);

  print("\nInsured name: $insuredName");
  for (var e in result) {
    if (e.insured?.name == insuredName) {
      double yearPremium =
          e.paymentFrequency == "yearly" ? e.insPremium : e.insPremium * 12;
      totalSum += yearPremium;

      print("Address: ${e.insured?.address}");
      print("Policy nº: ${e.policy}  || Yearly premium payed: $yearPremium");
    }
  }
  print("Total Amount: $totalSum\n");
}

void getInsuredInfo(List<Insurance> list) {
  List insuredList = [];
  for (var e in list) {
    if (e.insured != null) {
      if (!insuredList.contains(e.insured?.name)) {
        insuredList.add(e.insured?.name);
      }
    }
  }
  for (var i in insuredList) {
    printSummaryByInsured(list, i);
  }
}

// Owner
void printSummaryByOwner(list, ownerName) {
  List<Insurance> result;
  double totalSum = 0;

  result = getActivePolicies(list);

  print("\nOwner name: $ownerName");
  for (var e in result) {
    if (e.owner.name == ownerName) {
      double yearPremium =
          e.paymentFrequency == "yearly" ? e.insPremium : e.insPremium * 12;
      totalSum += yearPremium;

      print("Address: ${e.owner.address}");
      print("Policy nº: ${e.policy}  || Yearly premium payed: $yearPremium");
    }
  }
  print("Total Amount: $totalSum\n");
}

void getOwnerInfo(List<Insurance> list) {
  List ownerList = [];
  for (var e in list) {
      if (!ownerList.contains(e.owner.name)) {
        ownerList.add(e.owner.name);
      }
  }
  for (var i in ownerList) {
    printSummaryByOwner(list, i);
  }
}
