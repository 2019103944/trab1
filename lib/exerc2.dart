import 'package:testes/functions2.dart' as functions;
import 'dart:io';

void run() {
  bool menuOut = false;
  List<int> numbers = functions.getList();

  while (!menuOut) {
    print("\n\n---  Select one option  ---");
    print("\n---  1 - List size  ---");
    print("---  2 - Bigger element  ---");
    print("---  3 - Lower element  ---");
    print("---  4 - List amplitude  ---");
    print("---  5 - Odd Elements w/ order desc  ---");
    print("---  6 - Print All Elements  ---");
    print("---  7 - Exit ..  ---");
    String? option = stdin.readLineSync();

    switch (option) {
      case '1':
        print("\n========       List Size      ========");
        print(functions.getList().length);
        break;
      case '2':  
        print("\n========    Bigger Element    ========");
        print(functions.getBigger(numbers));
        break;
      case '3':
        print("\n========    Lower Element     ========");
        print(functions.getLower(numbers));
        break;
      case '4':
        print("\n========    List Amplitude    ========");
        print(functions.getBigger(numbers) - functions.getLower(numbers));
        break;
      case '5':
        print("\n========    Odd Elements    ========");
        print(functions.setOrder(functions.getOddNumbers(numbers)));
        break;
      case '6':
        print("\n========    Print All Elements    ========");
        print(functions.setOrder(numbers));
        break;
      case '7':
        print("Thanks. See you later ...");
        menuOut = true;
        break;
      default:
        print('Choose one option between 1 and 6 or 7 to exit program.');
    }
  }
}
