import 'package:testes/functions1.dart' as functions;

void run () {
  List<int> cousins = [];

  // Array push
  for (var i = 101; i < 200; i++) {
    functions.isCousinFast(i) ? cousins.add(i) : '';
  }

  print("\n========    Quantidade    ========");
  print("No intervalo ]100;200[ existem ${cousins.length} números primos.");

  print("\n===========    Soma    ===========");
  print(
      "No intervalo ]100;200[ existem números primos com uma soma de  ${functions.calculate(cousins)}.");

  print("\n===========    Média    ==========");
  print("No intervalo ]100;200[ existe números primos com uma média de ${functions.average(cousins)}.\n");
}