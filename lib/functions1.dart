import "dart:math" as math;

bool isCousin(int n) {
  for (int i = 2; i < n; i++) {
    int x = n % i;
    if (x == 0) {
      return false;
    }
  }
  return true;
}

bool isCousinFast(int n) {
  // Simple Cases
  if (n <= 1) {
    return false;
  }
  if (n <= 3) {
    return true;
  }

  if (n % 2 == 0 || n % 3 == 0) {
    return false;
  }

  for (int i = 5; i * i <= n; i = i + 6) {
    if (n % i == 0 || n % (i + 2) == 0) {
      return false;
    }
  }
  return true;
}

double average(List<int> cousins) {
  return (calculate(cousins) / cousins.length).roundToDouble();
}

int calculate(List<int> cousins) {
  int result = 0;
  for (var n in cousins) {
    result += n;
  }

  return result;
}
