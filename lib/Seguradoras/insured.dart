import 'package:testes/Seguradoras/person.dart';

class Insured extends Person {
  String ownerRelation;

  Insured(super.name, super.nif, super.address, super.age, this.ownerRelation);

  @override
  String toString() {
    return "${super.toString()}, ownerRelation: $ownerRelation";
  }
}
