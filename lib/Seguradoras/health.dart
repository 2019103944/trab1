import 'package:testes/Seguradoras/insurance.dart';

class Health extends Insurance {
  String examStatus;
  double hospitalCosts;

  Health(
    super.policy,
    super.creationAt,
    super.insPremium,
    super.status,
    super.coverageAmount,
    this.examStatus,
    this.hospitalCosts, 
    super.owner,
    super.company, {
    super.insured,
    super.paymentFrequency,
    super.deductible,
  });

  @override
  String toString() {
    return "${super.toString()}, examStatus: $examStatus, hospitalCosts: $hospitalCosts";
  }
}
