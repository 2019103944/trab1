import 'package:testes/Seguradoras/insurance.dart';

class House extends Insurance {
  double houseValue;
  String address;
  String houseType;

  House(
    super.policy,
    super.creationAt,
    super.insPremium,
    super.status,
    super.coverageAmount,
    this.houseValue,
    this.address,
    this.houseType, 
    super.owner,
    super.company, {
    super.insured,
    super.paymentFrequency,
    super.deductible,
  });

  @override
  String toString() {
    return "${super.toString()}, houseValue: $houseValue, houseType: $houseType, address: $address";
  }
}
