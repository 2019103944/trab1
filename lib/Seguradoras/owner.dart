import 'package:testes/Seguradoras/person.dart';

class Owner extends Person {
  int nib;

  Owner(super.name, super.nif, super.address, super.age, this.nib);

  @override
  String toString() {
    return "${super.toString()}, Iban: $nib";
  }
}
