import 'package:testes/Seguradoras/insuranceCompany.dart';
import 'package:testes/Seguradoras/insured.dart';
import 'package:testes/Seguradoras/owner.dart';

class Insurance {
  String policy;
  DateTime creationAt;
  double insPremium;
  String? paymentFrequency;
  double? deductible;
  bool status;
  double coverageAmount;
  Owner owner;
  InsuranceCompany company;
  Insured? insured;

  Insurance(
    this.policy,
    this.creationAt,
    this.insPremium,
    this.status,
    this.coverageAmount,
    this.owner,
    this.company, {
    this.insured,
    required paymentFrequency,
    this.deductible,
  });

  @override
  String toString() {
    return '''\nPolicy: $policy, Creation at: $creationAt,
     InsPremium: $insPremium, PaymentFrequency: ${paymentFrequency ?? "N/A"}, 
     Deductible: ${deductible ?? '0'}, Status: $status, CoverageAmount: $coverageAmount
   ''';
  }
}
