import 'package:testes/Seguradoras/insurance.dart';
import 'package:testes/Seguradoras/insured.dart';

class Automotive extends Insurance {
  String brand;
  String registration;
  Insured? usuallyDriver;
  String driverLicence;

  Automotive(
    super.policy,
    super.creationAt,
    super.insPremium,
    super.status,
    super.coverageAmount,
    super.owner,
    super.company,
    this.brand,
    this.registration,
    this.driverLicence, {
    super.insured,
    super.paymentFrequency,
    super.deductible,
    this.usuallyDriver
  });

  @override
  String toString() {
    return "${super.toString()} , brand: $brand, driverLicense: $driverLicence, registration: $registration";
  }
}
