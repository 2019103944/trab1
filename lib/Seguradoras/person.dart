class Person {
  String name;
  int nif;
  int age;
  String address;

  Person(this.name, this.nif, this.address, this.age);

  @override
  String toString() {
    return "Name: $name, Nif: $nif, Address: $address, Age: $age";
  }
}
