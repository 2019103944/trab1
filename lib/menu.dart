import 'dart:io';
import 'package:testes/exerc1.dart' as ex1;
import 'package:testes/exerc2.dart' as ex2;
import 'package:testes/exerc3.dart' as ex3;

void base() {
  bool menuOut = false;
  while (!menuOut) {
    print("\n-------   MENU   --------");

    print("\n\n---  Select one exercise  ---");
    print("\n---  1 - Exercise 1  ---");
    print("---  2 - Exercise 2  ---");
    print("---  3 - Exercise 3  ---");
    print("---  4 - Exit ..  ---");
    String? option = stdin.readLineSync();

    switch (option) {
      case '1':
        ex1.run();
        break;
      case '2':
        ex2.run();
        break;
      case '3':
        ex3.run();
        break;
      case '4':
        print("See you later ...");
        menuOut = true;
        break;
      default:
        print('Choose one option between 1 and 3 or 4 to exit program.');
    }
  }
}
