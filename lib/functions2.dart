import 'dart:math';

List<int> getList() {
  List<int> numbers = [];
  var rand = Random();
  for (int i = random(90, 151); i > 0; i--) {
    numbers.add(rand.nextInt(1000));
  }
  return numbers;
}

int random(int min, int max) {
  return min + Random().nextInt(max - min);
}

int getBigger(List<int> numbers) {
  int n = 0;
  for (var element in numbers) {
    if (element > n) {
      n = element;
    }
  }
  return n;
}

int getLower(List<int> numbers) {
  int n = getBigger(numbers);
  for (var element in numbers) {
    if (element < n) {
      n = element;
    }
  }
  return n;
}

List<int> getOddNumbers(List<int> numbers) {
  List<int> odd = [];
  for (var element in numbers) {
    if (element % 2 != 0) {
      odd.add(element);
    }
  }
  return odd;
}

List<int> setOrder(List<int> numbers) {
  numbers.sort((a, b) => b.compareTo(a));
  return numbers;
}
