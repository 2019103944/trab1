import 'package:testes/Seguradoras/insurance.dart';
import 'package:testes/functions3.dart';
import 'dart:io';

void run() {
  List health = createHealthList();
  List house = createHouseList();
  List auto = createListAutomotive();
  List<Insurance> all = [...health, ...house, ...auto];

  bool menuOut = false;

  while (!menuOut) {
    print("\n\n---  Select one option  ---");
    print("\n---  1 - Dashboard  ---");
    print("---  2 - Insert Insurance  ---");
    print("---  3 - Edit Insurance  ---");
    print("---  4 - Delete Insurance  ---");
    print("---  5 - Exit ..  ---");
    String? option = stdin.readLineSync();

    switch (option) {
      case '1':
        print("\n========       Dashboard      ========");
        // Print all elements
        printList(all);

        //Print Quantities
        print("\nHealth insurance quantity: ${createHealthList().length}");
        print("House insurance quantity: ${createHouseList().length}");
        print("Auto insurance quantity: ${createListAutomotive().length}");

        // Print Policies Status quantity
        print("\nAll Policies: ${getAllPoliciesQty(all)}");
        print("All active Policies: ${activePoliciesQty(all)}");
        print("All inactive Policies: ${inactivePoliciesQty(all)}");

        // Print Active Policies CoverAmount Average
        print(
            "\n\n===  Average and Qty of CoverageAmount (active policies) by Type ===");
        print("\n - HEALTH INSURANCE -");
        print("Qty: ${getAllPoliciesQty(health)}");
        print("Coverage Amount: ${getCoverageAmountAverage(health)}");

        print("\n - HOUSE INSURANCE -");
        print("Qty: ${getAllPoliciesQty(house)}");
        print("Coverage Amount: ${getCoverageAmountAverage(house)}");

        print("\n - AUTOMOTIVE INSURANCE -");
        print("Qty: ${getAllPoliciesQty(auto)}");
        print("Coverage Amount: ${getCoverageAmountAverage(auto)}");

        // Print Policy summary by Type and Insurance Company
        print("\n\n===  Policy summary by Type ===");
        printYearlySummaryByType(all);

        print("\n\n===  Policy summary by Company ===");
        printYearlySummaryByCompany(all);

        print("\n\n===  Policy summary by Entities ===");
        print("\n==  Insureds ==");
        getInsuredInfo(all);
        print("\n==  Owners ==");
        getOwnerInfo(all);
        break;
      case '2':  
        print("\n========    Insert Insurance    ========");
        print('');
        break;
      case '3':
        print("\n========    Edit Insurance     ========");
        print('');
        break;
      case '4':
        print("\n========    Delete Insurance    ========");
        print('');
        break;
      case '5':
        menuOut = true;
        break;
      default:
        print('Choose one option between 1 and 5 or 5 to exit program.');
    }
  }  
}
